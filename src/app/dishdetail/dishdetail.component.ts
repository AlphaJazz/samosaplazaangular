import { Component, OnInit } from '@angular/core';
import { DishService } from '../services/dish.service';

import { Params, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { switchMap } from 'rxjs/operators';

import {Dish} from '../shared/dish';
@Component({
  selector: 'app-dishdetail',
  templateUrl: './dishdetail.component.html',
  styleUrls: ['./dishdetail.component.scss']
})
export class DishdetailComponent implements OnInit {
  //@Input()
  dish: Dish;
  dishIds:number[];
  prev:number;
  next:number;
  
    constructor(private dishservice: DishService,
      private route: ActivatedRoute,
      private location: Location) { }
  
    ngOnInit() {
     /* let id = +this.route.snapshot.params['id'];
       this.dishservice.getDish(id)
      .subscribe(dish => this.dish=dish);*/

      this.dishservice.getDishIds().subscribe(dishIds => this.dishIds = dishIds);
      this.route.params.pipe(switchMap((params: Params) => this.dishservice.getDish(+params['id'])))
      .subscribe(dish => { this.dish = dish; this.setPrevNext(dish.id); });
    }
    setPrevNext(dishId: number) {
      const index = this.dishIds.indexOf(dishId);
      this.prev = this.dishIds[(this.dishIds.length + index - 1) % this.dishIds.length];
      this.next = this.dishIds[(this.dishIds.length + index + 1) % this.dishIds.length];
    }
  
    goBack(): void {
      this.location.back();
    }
}


/*
The above code has quite a large meaning,here the snapshot fetch the 
dish id at a given instant of the time.
for eg :: here the + has larger meaning . it conver the acquired string into
  a number.
the params take the parameter as an ID, interestingly it has larger impat on its own

now we are using another convenient away to achieve so...
the above code needs more proper explanation then you think;
*/