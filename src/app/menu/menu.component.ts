import { Component, OnInit } from '@angular/core';

import { Dish } from '../shared/dish';
//import { DISHES } from '../shared/dishes';
//the above two import has been replaced through the help of DishServices;

import { DishService } from '../services/dish.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

 dishes:Dish[];
 //below code is creating an object named selected Dish of precisely Dish class.

 selectedDish:Dish;
  constructor( private dishService: DishService) { }
//it can ask the services to fetch the information 

  ngOnInit() {
    this.dishService.getDishes().subscribe( dishes => this.dishes=dishes);
  }
  onSelect(dish:Dish){
    this.selectedDish=dish;
  }

}
