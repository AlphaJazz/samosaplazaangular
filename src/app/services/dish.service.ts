import { Injectable } from '@angular/core';
import { Dish } from '../shared/dish';
import { DISHES } from '../shared/dishes';
import { Observable } from 'rxjs/Observable';

import { of } from 'rxjs/observable/of';
import { delay } from 'rxjs/operators';

import 'rxjs/add/operator/delay';



@Injectable()
export class DishService {

  constructor() { }

  getDishes(): Observable<Dish[]> {
    return of(DISHES).pipe(delay(2000));
  }


  getDish(id: number): Observable<Dish> {
    return of(DISHES.filter((dish) => (dish.id === id))[0]).pipe(delay(2000));
  }

  getFeaturedDish(): Observable<Dish> {
    return of(DISHES.filter((dish) => dish.featured)[0]).pipe(delay(2000));
  }

  getDishIds(): Observable<number[] | any> {
    return of(DISHES.map(dish => dish.id));
  }
  
}


//we are trying to handle the deelay ... if u dont want then just
// replace the return part with return Promises.resolve(DISHES)
//here we will deal with only dishes 
//now we are handling the above part through observables.
// http in angular relied upon observable